class FocusAPIError(Exception):

    def __init__(self, expression, message, code=None):
        self.expression = expression
        self.message = message
        self.code = code
