import json

import requests

from .errors import FocusAPIError
from .utils import Singleton


PRODUCTION = 1
HOMOLOG = 2

FOCUS_BASE_URLS = {
    PRODUCTION: 'https://api.focusnfe.com.br/v2',
    HOMOLOG: 'http://homologacao.acrasnfe.acras.com.br/v2'
}

FOCUS_RAW_PATHS = {
    'mde': {
        'envia_manifesto': 'nfes_recebidas/{key}/manifesto',
        'busca': 'nfes_recebidas',
        'consulta_nota': 'nfes_recebidas/{key}',
        'consulta_manifesto': 'nfes_recebidas/{key}/manifesto',
        'consulta_info_json': 'nfes_recebidas/{key}.json',
        'consulta_info_xml': 'nfes_recebidas/{key}.xml',
        'cancelamento': 'nfes_recebidas/{key}/cancelamento.xml',
        'carta_correcao': 'nfes_recebidas/{key}/carta_correcao.xml'
    },
    'nfce': {
        'criar': 'nfce',
        'consultar': 'nfce/{ref}',
        'cancelar': 'nfce/{ref}',
        'enviar_email': 'nfce/{ref}/email',
        'inutilizacao': 'nfce/{ref}/inutilizacao',
    },
    'nfe': {
        'criar': 'nfe',
        'consultar': 'nfe/{ref}',
        'cancelar': 'nfe/{ref}',
        'carta_correcao': 'nfe/{ref}/carta_correcao',
        'enviar_email': 'nfe/{ref}/email',
        'inutilizacao': 'nfe/{ref}/inutilizacao',
    },
    'nfse': {
        'criar': 'nfse',
        'consultar': 'nfse/{ref}',
        'cancelar': 'nfse/{ref}',
        'enviar_email': 'nfse/{ref}/email',
    }
}

UNKNOWN = 'UNKNOWN'


class FocusNFeAPI(metaclass=Singleton):

    token, obj, cnpj = None, None, ''

    def __init__(self, environ=HOMOLOG):
        self.environ = environ if environ in [PRODUCTION, HOMOLOG] else HOMOLOG

    def set_token(self, token):
        self.token = token
        return self

    def set_cnpj(self, cnpj):
        self.cnpj = cnpj
        return self

    def set_obj(self, obj):
        self.obj = obj
        return self

    def set_environ(self, new_environ):
        self.environ = new_environ
        return self

    def _make_url(self, service, **kwargs):
        base_url = FOCUS_BASE_URLS[self.environ]
        try:
            raw_path = FOCUS_RAW_PATHS[self.obj.name][service]
            params = {**self.obj.to_dict(), **kwargs}
            path = raw_path.format(**params)
            return '{}/{}'.format(base_url, path)
        except KeyError:
            return UNKNOWN

    @staticmethod
    def _handle_response(response):
        url = response.url
        status = response.status_code
        if status in [200, 201]:
            return json.loads(response.text) if not url.split('?')[0].endswith('.xml') else response.content
        elif status == 404:
            raise FocusAPIError('Not Found', 'Request URL: {}'.format(url))
        elif status == 401:
            raise FocusAPIError('Not Authorized', 'Request URL: {}'.format(url))
        elif status == 400:
            raise FocusAPIError('Bad Request', 'Raise: {}'.format(response.text), code=400)

    def _request(self, url, method='GET', **kwargs):
        if url == UNKNOWN:
            raise FocusAPIError('Unknown Service', 'Request URL: {}'.format(url))
        if method == 'GET':
            return self._handle_response(requests.get(url, kwargs, auth=(self.token, "")))
        elif method == 'POST':
            return self._handle_response(requests.post(url, json.dumps(kwargs), auth=(self.token, "")))
        elif method == 'DELETE':
            return self._handle_response(requests.delete(url, auth=(self.token, "")))
        elif method == 'UPDATE':
            return self._handle_response(requests.put(url, data=json.dumps(kwargs), auth=(self.token, "")))

    def get(self, service, **kwargs):
        url = self._make_url(service, **kwargs)
        return self._request(url, **kwargs)

    def post(self, service, **kwargs):
        url = self._make_url(service, **kwargs)
        return self._request(url, method='POST', **kwargs['data'])

    def delete(self, service):
        url = self._make_url(service)
        return self._request(url, method='DELETE')

    def update(self, service, **kwargs):
        url = self._make_url(service)
        return self._request(url, method='UPDATE', **kwargs)
