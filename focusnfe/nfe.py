from .baseapi import BaseAPI


class NFe(BaseAPI):
    name = 'nfe'

    def create_nfe(self, **kwargs):
        """
        Cria uma nota fiscal e a envia para processamento
        :return:
        """
        return self.api.post('criar', **kwargs)

    def consult_nfe(self):
        """
        Consulta a nota fiscal com a referência informada e o seu status de processamento
        :return:
        """
        return self.api.get('consultar')

    def cancel_nfe(self):
        """
        Cancela uma nota fiscal com a referência informada
        :return:
        """
        return self.api.delete('cancelar')

    def create_carta_de_correcao(self):
        """
        Cria uma carta de correção para a nota fiscal com a referência informada.
        :return:
        """
        return self.api.post('carta_correcao')

    def send_email_with_nfe_copy(self):
        """
        Envia um email com uma cópia da nota fiscal com a referência informada.
        :return:
        """
        return self.api.post('enviar_email')

    def disablement_nfe(self):
        """
        Inutiliza uma numeração da nota fiscal.
        :return:
        """
        return self.api.post('inutilizacao')
