from .baseapi import BaseAPI


class NFCe(BaseAPI):
    name = 'nfce'

    def create_nfce(self):
        """
        Cria uma nota fiscal e a envia para processamento.
        :return:
        """
        return self.api.post('criar')

    def consult_nfce(self):
        """
        Consulta a nota fiscal com a referência informada e o seu status de processamento.
        :return:
        """
        return self.api.get('consultar')

    def cancel_nfce(self):
        """
        Cancela uma nota fiscal com a referência informada.
        :return:
        """
        return self.api.delete('cancelar')

    def send_email_with_nfce_copy(self):
        """
        Envia um email com uma cópia da nota fiscal com a referência informada.
        :return:
        """
        return self.api.post('enviar_email')

    def disablement_nfce(self):
        """
        Inutiliza uma numeração da nota fiscal.
        :return:
        """
        return self.api.post('inutilizacao')
