from .baseapi import BaseAPI


class MDe(BaseAPI):
    name = 'mde'

    def send_manifesto(self, key, data):
        """
        Realiza um manifesto na nota informada.
        :return:
        """
        return self.api.post('envia_manifesto', key=key, data=data)

    def search(self):
        """
        Busca informações resumidas de todas as NFe’s recebidas.
        :return:
        """
        return self.api.get('busca', **{'cnpj': self.cnpj})

    def consult_manifesto(self, key):
        """
        Consulta o último manifesto válido na nota fiscal informada.
        :return:
        """
        return self.api.get('consulta_manifesto', key=key)

    def consult_nfe(self, key):
        """
        Consulta o último manifesto válido na nota fiscal informada.
        :return:
        """
        return self.api.get('consulta_nota', key=key, completa=1)

    def consult_info_json(self):
        """
        Consulta as informações da nota fiscal em formato JSON.
        :return:
        """
        return self.api.get('consulta_info_json')

    def consult_info_xml(self, key):
        """
        Consulta as informações da nota fiscal em formato XML.
        :return:
        """
        return self.api.get('consulta_info_xml', key=key)

    def cancel(self):
        """
        Se existir, baixa o XML de cancelamento da nota fiscal informada.
        :return:
        """
        return self.api.get('cancelamento')

    def download_xml(self):
        """
        Se existir, baixa o XML da última carta de correção da nota fiscal informada.
        :return:
        """
        return self.api.get('carta_correcao')
