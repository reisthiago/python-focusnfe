import unittest

from errors import FocusAPIError
from focusnfe import FocusNFeAPI, MDe


class TestMDe(unittest.TestCase):

    def setUp(self):
        self.valid_token = 'XNna5o04Px6stFG20EsTs3fvSb9zdp2p'
        self.invalid_token = 'XXX_TOKEN_INVALIDO'
        self.cnpj = '41336116000183'
        self.invalid_cnpj = '0000000000000'
        self.obj = MDe()

    def test_consultar_todos_manifestos_com_token_invalido(self):
        FocusNFeAPI().set_token(self.invalid_token).set_cnpj(self.cnpj)
        self.assertEqual(FocusNFeAPI().token, self.invalid_token)
        self.assertRaises(FocusAPIError, lambda: self.obj.search())

    def test_consultar_todos_manifestos_com_cnpj_invalido(self):
        FocusNFeAPI().set_token(self.valid_token).set_cnpj(self.invalid_cnpj)
        self.assertEqual(FocusNFeAPI().cnpj, self.invalid_cnpj)
        self.assertRaises(FocusAPIError, lambda: self.obj.search())

    def test_consultar_todos_manifestos_com_token_valido(self):
        FocusNFeAPI().set_token(self.valid_token).set_cnpj(self.invalid_token)
        self.assertEqual(FocusNFeAPI().token, self.valid_token)
        result = self.obj.search()
        self.assertEqual(type(result), list)
