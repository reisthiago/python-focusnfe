from .baseapi import BaseAPI


class NFSe(BaseAPI):
    name = 'nfse'

    def create_nfe(self):
        """
        Cria uma nota fiscal e a envia para processamento
        :return:
        """
        return self.api.post('criar')

    def consult_nfe(self):
        """
        Consulta a nota fiscal com a referência informada e o seu status de processamento
        :return:
        """
        return self.api.get('consultar')

    def cancel_nfe(self):
        """
        Cancela uma nota fiscal com a referência informada
        :return:
        """
        return self.api.delete('cancelar')

    def send_email_with_nfe_copy(self):
        """
        Envia um email com uma cópia da nota fiscal com a referência informada.
        :return:
        """
        return self.api.post('enviar_email')

