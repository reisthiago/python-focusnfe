from .focusnfe_api import FocusNFeAPI


class BaseAPI:
    name = None

    def __init__(self):
        self.api = FocusNFeAPI().set_obj(self)
        self.cnpj = self.api.cnpj

    def to_dict(self):
        return vars(self)


