# -*- coding: utf-8 -*-
from distutils.core import setup

import focusnfe

if __name__ == "__main__":
    setup(
        name='focusnfe',
        version=focusnfe.__version__,
        description='Focus NFe API',
        # long_description=open('README').read(),
        author='Thiago Reis',
        author_email='thiago@enko.com.br',
        packages=[
            'focusnfe',
            'focusnfe.tests',
        ],
        install_requires=['requests']
    )
